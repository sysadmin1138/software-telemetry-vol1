# Software Telemetry: Reliable Logging and Monitoring

Welcome to the repository for Software Telemetry. This hosts supplemental
resources supporting [the book][the book], including all code found in
listings. This book is a bit different than many Manning books, in that
I don't teach you how to build a thing from the ground up. Instead, I teach
you how to improve what you already have. I use code to illustrate specific
techniques. It is entirely safe to skip around between chapters, though
code within a chapter often builds on previous listings.

## Programming languages and frameworks
The code here is a mix of source-code and configuration files for
various platorms and products. The majority of code and configs will be
for:

* Python: works with 3.7 and 3.9
* Elastic.co Filebeat: 7.*
* Elastic.co Logstash: 7.*
* Ruby (for chapters 11 and 16): works with 2.7 through 3.0

**Note**: Thanks to the efforts of Jean-François Morin I am able
to provide Java-based examples in addition to the listings provided
in the book. I am not a Java person, but Jean-François is, and their
efforts are greatly appreciated.

## Python modules used
The chapters here use a variety of Python modules to teach telemetry
concepts.

* [cryptography][py-crypto] - Used in [chapter 13's][ch13] discussion of
 the role of encryption in telemetry.
* [elasticsearch][py-elasticsearch] - Used in [chapter 3's][ch3] discussion
 of the Shipping Stage and how telemetry moves.
* [opentelemetry-api][py-ot] and [opentelemetry-sdk][py-ot] - Used in 
[chapter 6's][ch6] discussion on marking up and enriching telemetry.
* [redis][py-redis] - Used in [chapter 3's][ch3] discussion of Shipping Stage
 telemetry movement, and [chapter 6's][ch6] description of marking up
 telemetry.
* [rlog][py-rlog] - Used in [chapter 12's][ch12] discussion of structured
 logging,  used to allow emitting structured logging events into a Redis
 list.
* [structlog][py-structlog] - Used in [chapters 12][ch12] and [13][ch13] 
to demonstrate structured logging concepts.

[py-crypto]: https://pypi.org/project/cryptography/
[py-elasticsearch]: https://pypi.org/project/elasticsearch/
[py-ot]: https://opentelemetry-python.readthedocs.io/en/latest/
[py-redis]: https://pypi.org/project/redis/
[py-rlog]: https://github.com/lobziik/rlog
[py-structlog]: https://www.structlog.org/en/stable/

## Ruby modules used
[Chapter 11][ch11] uses Ruby to demonstrate regular expressions because the two
major Shipping Stage engines, [Logstash][logstash] and [FluentD][fluentd]
are both Ruby projects (or started as Ruby). [Chapter 16][ch16] contains more
examples drawn from my history of reprocessing Elasticsearch-based 
telemetry using Ruby.

* [benchmark][gem-benchmark] - Used to capture regular expression performance.
* [elasticsearch][gem-elasticsearch] - Used in [chapter 16's][ch16]
 discussion of reprocessing telemetry.
* [json][gem-json] - Used in [chapter 16's][ch16] discussion of reprocessing
 telemetry.
* [OpenSSL][gem-openssl] - Used in [chapter 15's][ch15] examination of how
to validate a cryptographic hash using [Logstash][logstash].
* [rest-client][gem-rest] - Used in [chapter 16's][ch16] discussion of
 reprocessing telemetry.

[fluentd]: https://docs.fluentd.org/
[gem-benchmark]: https://www.rubydoc.info/gems/benchmark
[gem-elasticsearch]: https://github.com/elastic/elasticsearch-ruby
[gem-json]: https://ruby-doc.org/stdlib-3.0.0/libdoc/json/rdoc/JSON.html
[gem-openssl]: https://ruby-doc.org/stdlib-3.0.0/libdoc/openssl/rdoc/OpenSSL.html
[gem-rest]: https://www.rubydoc.info/gems/rest-client

[the book]: https://www.manning.com/books/software-telemetry
[logstash]: https://www.elastic.co/guide/en/logstash/current/introduction.html
[ch3]: https://livebook.manning.com/book/software-telemetry/chapter-3
[ch6]: https://livebook.manning.com/book/software-telemetry/chapter-6
[ch11]: https://livebook.manning.com/book/software-telemetry/chapter-11
[ch12]: https://livebook.manning.com/book/software-telemetry/chapter-12
[ch13]: https://livebook.manning.com/book/software-telemetry/chapter-13
[ch15]: https://livebook.manning.com/book/software-telemetry/chapter-15
[ch16]: https://livebook.manning.com/book/software-telemetry/chapter-16
