import re
import json

test_string="timestamp='2020-09-24T00:43:37.659549Z' metric_name='pdf_pages' metric_type='c' metric_value=3 payment_plan='alpha' hostname='k8s-14.euc1.prod.internal' pid=19661 release_id='0.7.1' commit='f0d00b1' level='info'"

splitter = re.compile("(\S+)=(\S+)")
quote_chars = "'\""

fields = {}
for kv in splitter.findall(test_string):
    if kv[1][0] in quote_chars:
        trim_v = kv[1][0]
        value = kv[1].strip(trim_v).rstrip(trim_v)
    else:
        if '.' in kv[1]:
            value = float(kv[1])
        else:
            value = int(kv[1])

    fields[kv[0]] = value

print(json.dumps(fields))
