import logging
import datetime
import sys
import socket
import os
from structlog import (
    get_logger,
    configure,
)
from structlog.stdlib import (
    LoggerFactory,
    BoundLogger,
    add_log_level
)
from structlog.processors import (
    KeyValueRenderer,
    JSONRenderer,
    UnicodeDecoder,
    TimeStamper
)

logging.basicConfig(
    format="%(message)s",
    stream=sys.stdout,
    level=logging.INFO,
)

__release__ = "0.7.1"
__commit__ = "f0d00b1"

configure(
    processors=[
        TimeStamper(fmt="iso"),
        UnicodeDecoder(),
        add_log_level,
        KeyValueRenderer(key_order=[
            'timestamp', 'metric_name',
            'metric_type', 'metric_value'])
    ],
    context_class=dict,
    logger_factory=LoggerFactory(),
    wrapper_class=BoundLogger,
    cache_logger_on_first_use=False
)

logger = get_logger()

def __add_context(event):
    event = event.bind(
        hostname=socket.gethostname(),
        pid=os.getpid(),
        release_id=__release__,
        commit=__commit__
        )
    return event

def __do_metric(metric, value, mtype, metadata=[], fields=[]):
    event = logger.bind(
        metric_name=metric,
        metric_value=value,
        metric_type=mtype)
    context_fields = {}
    for f in fields:
        if f in metadata:
            context_fields[f]=metadata[f]

    event = event.bind(**context_fields)
    event = __add_context(event)
    return event

def counter(msg, value, metadata=[], fields=[]):
    event = __do_metric(msg, value, 'c', metadata, fields)
    event.info()

def timer(msg, value, metadata=[], fields=[]):
    event = __do_metric(msg, value, 't', metadata, fields)
    event.info()

metadata1 = {'payment_plan': 'alpha', 'account_id': '1121'}
metadata2 = {'payment_plan': 'thunderdome', 'account_id': '23b9c1'}
metadata3 = {'payment_plan': 'skyfall', 'account_id': 'a3953021'}

counter('pdf_pages', 3, metadata1, ['payment_plan'])
counter('pdf_pages', 19, metadata2, ['payment_plan', 'account_id'])
timer('page_convert_time', 0.92, metadata3, ['account_id'])
timer('page_convert_time', 1.22)
