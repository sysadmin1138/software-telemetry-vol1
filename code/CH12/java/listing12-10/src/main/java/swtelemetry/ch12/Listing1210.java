package swtelemetry.ch12;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Jean-François Morin
 */
public class Listing1210 {

  public static void main(String[] args) {
    String testString = "timestamp='2020-09-24T00:43:37.659549Z' metric_name='pdf_pages' metric_type='c' metric_value=3 payment_plan='alpha' hostname='k8s-14.euc1.prod.internal' pid=19661 release_id='0.7.1' commit='f0d00b1' level='info'";

    Pattern splitter = Pattern.compile("(\\S+)=(\\S+)");
    String quoteChars = "'\"";

    Map<String, Object> fields = new LinkedHashMap<>();
    Matcher matcher = splitter.matcher(testString);
    while (matcher.find()) {
      String kv0 = matcher.group(1);
      String kv1 = matcher.group(2);
      Object value = "";
      if (quoteChars.indexOf(kv1.charAt(0)) > -1) {
        String trimmed = kv1;
        trimmed = trimmed.substring(1);
        value = trimmed.substring(0, trimmed.length() - 1);
      } else {
        if (kv1.indexOf('.') > -1) {
          value = Double.valueOf(kv1);
        } else {
          value = Long.valueOf(kv1);
        }
      }
      fields.put(kv0, value);
    }

    ObjectMapper objectMapper = new ObjectMapper();
    try {
      String json = objectMapper.writeValueAsString(fields);
      System.out.println(json);
    }
    catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }

}
