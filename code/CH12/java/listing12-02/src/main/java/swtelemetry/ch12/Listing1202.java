package swtelemetry.ch12;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ObjectMessage;

/**
 * @author Jean-François Morin
 */
public class Listing1202 {

  private static final Logger LOGGER = LogManager.getLogger("CONSOLE_JSON_APPENDER");
  private static final String RELEASE = "0.7.1";
  private static final String COMMIT = "f0d00b1";

  private static String getHostName() {
    try {
      return InetAddress.getLocalHost().getHostName();
    }
    catch (UnknownHostException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  private static void addContext(Map<String, Object> event) {
    event.put("hostname", getHostName());
    event.put("pid", ProcessHandle.current().pid());
    event.put("release_id", RELEASE);
    event.put("commit", COMMIT);
  }

  private static Map<String, Object> doMetric(String metric, Object value, String mtype, Map<String, ?> metadata, List<String> fields) {
    Map<String, Object> map = new HashMap<>();
    map.put("metric_name", metric);
    map.put("metric_value", value);
    map.put("metric_type", mtype);
    if (fields != null && metadata != null) {
      fields.stream().filter(metadata::containsKey).forEach(f -> map.put(f, metadata.get(f)));
    }
    addContext(map);
    return map;
  }

  public static void counter(String msg, Object value) {
    counter(msg, value, null, null);
  }

  public static void counter(String msg, Object value, Map<String, ?> metadata, List<String> fields) {
    Map<String, Object> event = doMetric(msg, value, "c", metadata, fields);
    ObjectMessage objMsg = new ObjectMessage(event);
    LOGGER.info(objMsg);
  }

  public static void timer(String msg, Object value) {
    timer(msg, value, null, null);
  }

  public static void timer(String msg, Object value, Map<String, ?> metadata, List<String> fields) {
    Map<String, Object> event = doMetric(msg, value, "t", metadata, fields);
    ObjectMessage objMsg = new ObjectMessage(event);
    LOGGER.info(objMsg);
  }

  public static void main(String[] args) {
    var metadata1 = Map.of("payment_plan", "alpha", "account_id", "1121");
    var metadata2 = Map.of("payment_plan", "thunderdome", "account_id", "23b9c1");
    var metadata3 = Map.of("payment_plan", "skyfall", "account_id", "a3953021");

    counter("pdf_pages", 3, metadata1, List.of("payment_plan"));
    counter("pdf_pages", 19, metadata2, List.of("payment_plan", "account_id"));
    timer("page_convert_time", 0.92, metadata3, List.of("account_id"));
    timer("page_convert_time", 1.22);
  }

}
