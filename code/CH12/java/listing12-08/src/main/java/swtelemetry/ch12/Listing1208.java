package swtelemetry.ch12;

import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Jean-François Morin
 */
public class Listing1208 {

  public static void main(String[] args) {
    String testString = """
            2023-02-19T14:41:22.918238Z Profile image updated -- {"hostname": "k8s-14.euc1.prod.internal", "pid": 23030, "release": "0.7.1", "commit": "f0d00b1",  "account_id": "1515323", "feature_flags": {"new_login": true, "new_profile": false}, "level": "info"}""";

    String plainPart = "";
    String jsonPart = "";
    int indexSeparator = testString.indexOf(" -- ");
    if (indexSeparator > -1) {
      plainPart = testString.substring(0, indexSeparator);
      jsonPart = testString.substring(indexSeparator + 4);
    } else {
      plainPart = testString;
    }

    String timestamp = "";
    String event;
    indexSeparator = plainPart.indexOf(' ');
    timestamp = plainPart.substring(0, indexSeparator);
    event = plainPart.substring(indexSeparator + 1);

    ObjectMapper objectMapper = new ObjectMapper();
    try {
      @SuppressWarnings("unchecked")
      Map<String, Object> fields = objectMapper.readValue(jsonPart, Map.class);
      fields.put("event", event);
      fields.put("timestamp", timestamp);

      String json = objectMapper.writeValueAsString(fields);
      System.out.println(json);
    }
    catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }

}
