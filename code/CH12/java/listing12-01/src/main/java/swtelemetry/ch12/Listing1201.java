package swtelemetry.ch12;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.MapMessage;

/**
 * @author Jean-François Morin
 */
public class Listing1201 {

  private static final Logger LOGGER = LogManager.getLogger("CONSOLE_JSON_APPENDER");
  private static final String RELEASE = "0.7.1";
  private static final String COMMIT = "f0d00b1";

  private static String getHostName() {
    try {
      return InetAddress.getLocalHost().getHostName();
    }
    catch (UnknownHostException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  private static void addContext(Map<String, Object> event) {
    event.put("hostname", getHostName());
    event.put("pid", ProcessHandle.current().pid());
    event.put("release_id", RELEASE);
    event.put("commit", COMMIT);
  }

  private static Map<String, Object> doMetric(String metric, Object value, String mtype, Map<String, ?> metadata) {
    Map<String, Object> map = new HashMap<>();
    map.put("payment_plan", metadata.get("payment_plan"));
    map.put("metric_name", metric);
    map.put("metric_value", value);
    map.put("metric_type", mtype);
    addContext(map);
    return map;
  }

  public static void counter(String msg, Object value, Map<String, ?> metadata) {
    Map<String, Object> event = doMetric(msg, value, "c", metadata);
    MapMessage<?, ?> objMsg = new MapMessage<>(event);
    LOGGER.info(objMsg);
  }

  public static void timer(String msg, Object value, Map<String, ?> metadata) {
    Map<String, Object> event = doMetric(msg, value, "t", metadata);
    MapMessage<?, ?> objMsg = new MapMessage<>(event);
    LOGGER.info(objMsg);
  }

  public static void main(String[] args) {
    counter("pdf_pages", 3, Map.of("payment_plan", "alpha"));
    counter("pdf_pages", 19, Map.of("payment_plan", "thunderdome"));
    timer("page_convert_time", 0.92, Map.of("payment_plan", "alpha"));
  }

}
