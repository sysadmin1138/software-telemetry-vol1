package swtelemetry.ch12;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ObjectMessage;

/**
 * @author Jean-François Morin
 */
public class Listing1206 {

  private static final Logger LOGGER = LogManager.getLogger("RedisAppender");
  private static final String RELEASE = "0.7.1";
  private static final String COMMIT = "f0d00b1";

  private static String getHostName() {
    try {
      return InetAddress.getLocalHost().getHostName();
    }
    catch (UnknownHostException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  private static void addContext(Map<String, Object> event) {
    event.put("hostname", getHostName());
    event.put("pid", ProcessHandle.current().pid());
    event.put("release_id", RELEASE);
    event.put("commit", COMMIT);
  }

  private static Map<String, Object> filterMetadata(Map<String, ?> metadata, List<String> fields) {
    Map<String, Object> map = new HashMap<>();
    if (fields != null && metadata != null) {
      fields.stream().filter(metadata::containsKey).forEach(f -> map.put(f, metadata.get(f)));
    }
    return map;
  }

  private static Map<String, Object> mergeContext(Map<String, ?> metadata, List<String> fields) {
    Map<String, Object> map = new HashMap<>();
    addContext(map);
    Map<String, Object> filterContext = filterMetadata(metadata, fields);
    map.putAll(filterContext);
    return map;
  }

  public static void info(String eventData, Map<String, ?> metadata, List<String> fields) {
    Map<String, Object> event = mergeContext(metadata, fields);
    event.put("event", eventData);
    event.put("timestamp", Instant.now());
    ObjectMessage objMsg = new ObjectMessage(event);
    LOGGER.info(objMsg);
  }

  public static void warning(String eventData, Map<String, ?> metadata, List<String> fields) {
    Map<String, Object> event = mergeContext(metadata, fields);
    event.put("event", eventData);
    event.put("timestamp", Instant.now());
    ObjectMessage objMsg = new ObjectMessage(event);
    LOGGER.warn(objMsg);
  }

  public static void error(String eventData, Map<String, ?> metadata, List<String> fields) {
    Map<String, Object> event = mergeContext(metadata, fields);
    event.put("event", eventData);
    event.put("timestamp", Instant.now());
    ObjectMessage objMsg = new ObjectMessage(event);
    LOGGER.error(objMsg);
  }

  public static void main(String[] args) {
    var metadata = Map.of(
        "account_id", "1515323",
        "payment_plan", "Enterprise Plus",
        "region", "euc1",
        "feature_flags", Map.of("new_login", Boolean.TRUE, "new_profile", Boolean.FALSE)
    );

    info("Profile image updated", metadata, List.of("account_id", "feature_flags"));
  }

}
