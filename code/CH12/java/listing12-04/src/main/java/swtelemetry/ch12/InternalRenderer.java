package swtelemetry.ch12;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.Node;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginBuilderAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginBuilderFactory;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.core.layout.AbstractStringLayout;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.message.Message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Plugin(name = "InternalRendererLayout", category = Node.CATEGORY, elementType = Layout.ELEMENT_TYPE, printObject = true)
public class InternalRenderer extends AbstractStringLayout {

  private static final String DEFAULT_FOOTER = "]";
  private static final String DEFAULT_HEADER = "[";
  private ObjectMapper dumps;
  protected final boolean compact;

  public static class Builder<B extends Builder<B>> extends AbstractStringLayout.Builder<B>
      implements org.apache.logging.log4j.core.util.Builder<InternalRenderer> {

    private ObjectMapper serializer = new ObjectMapper();
    @PluginBuilderAttribute
    private boolean compact;

    protected String toStringOrNull(final byte[] header) {
      return header == null ? null : new String(header, Charset.defaultCharset());
    }

    @Override
    public InternalRenderer build() {
      String headerPattern = toStringOrNull(getHeader());
      String footerPattern = toStringOrNull(getFooter());
      return new InternalRenderer(serializer, compact, getConfiguration(), headerPattern, footerPattern, getCharset());
    }

  }

  private InternalRenderer(ObjectMapper serializer, boolean compact, Configuration config,
      String headerPattern, String footerPattern, Charset charset) {
    super(config, charset,
        PatternLayout.newSerializerBuilder().setConfiguration(config).setPattern(headerPattern).setDefaultPattern(DEFAULT_HEADER).build(),
        PatternLayout.newSerializerBuilder().setConfiguration(config).setPattern(footerPattern).setDefaultPattern(DEFAULT_FOOTER).build());
    dumps = serializer;
    this.compact = compact;
  }

  @PluginBuilderFactory
  public static <B extends Builder<B>> B newBuilder() {
    return new Builder<B>().asBuilder();
  }

  @Override
  public String toSerializable(LogEvent event) {
    dumps.configure(SerializationFeature.INDENT_OUTPUT, !compact);

    Log4jLogEvent log4jEvent = convertMutableToLog4jEvent(event);
    Message message = log4jEvent.getMessage();
    Object[] params = message.getParameters();
    @SuppressWarnings("unchecked")
    Map<String, ?> eventDict = params == null || params.length == 0 ? new HashMap<>() : (Map<String, ?>) message.getParameters()[0];

    Object paramTimestamp = eventDict.remove("timestamp");
    Object paramEvent = eventDict.remove("event");
    try {
      String contextJson = dumps.writeValueAsString(eventDict);
      return paramTimestamp + " " + paramEvent + " -- " + contextJson;
    }
    catch (JsonProcessingException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  private static Log4jLogEvent convertMutableToLog4jEvent(LogEvent event) {
    return (Log4jLogEvent) (event instanceof Log4jLogEvent ? event : Log4jLogEvent.createMemento(event));
  }

}
