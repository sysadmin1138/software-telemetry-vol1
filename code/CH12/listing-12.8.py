import json

test_string = '2023-02-19T14:41:22.918238Z Profile image updated -- {"hostname": "k8s-14.euc1.prod.internal", "pid": 23030, "release": "0.7.1", "commit": "f0d00b1",  "account_id": "1515323", "feature_flags": {"new_login": true, "new_profile": false}, "level": "info"}'

if ' -- ' in test_string:
    plain_part, json_part = test_string.split(' -- ', 1)
else:
    plain_part = test_string
    json_part = ''

timestamp, event = plain_part.split(' ', 1)
fields = json.loads(json_part)
fields['event'] = event
fields['timestamp'] = timestamp
print(json.dumps(fields))
