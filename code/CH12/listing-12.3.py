import logging
import datetime
import sys
import socket
import os
from structlog import (
    get_logger,
    configure,
)
from structlog.stdlib import (
    LoggerFactory,
    BoundLogger,
    add_log_level,
)
from structlog.processors import (
    KeyValueRenderer,
    JSONRenderer,
    UnicodeDecoder,
    TimeStamper
)

logging.basicConfig(
    format="%(message)s",
    stream=sys.stdout,
    level=logging.INFO,
)

__release__ = "0.7.1"
__commit__ = "f0d00b1"

configure(
    processors=[
        TimeStamper(fmt="iso"),
        UnicodeDecoder(),
        add_log_level,
        JSONRenderer(),
    ],
    context_class=dict,
    logger_factory=LoggerFactory(),
    wrapper_class=BoundLogger,
    cache_logger_on_first_use=False
)

logger = get_logger()

def __add_context():
    context = {
        'hostname': socket.gethostname(),
        'pid': os.getpid(),
        'release': __release__,
        'commit': __commit__
    }
    return context

def __filter_metadata(metadata, fields):
    fcontext = {}
    for f in fields:
        if f in metadata:
            fcontext[f] = metadata[f]

    return fcontext

def __merge_context(metadata, fields):
    base_context = __add_context()
    filter_context = __filter_metadata(metadata, fields)
    merged_context = {**base_context, **filter_context}
    return merged_context

def info(eventdata, metadata=[], fields=[]):
    event = logger.bind(**__merge_context(metadata, fields))
    event.info(eventdata)

def warning(eventdata, metadata=[], fields=[]):
    event = logger.bind(**__merge_context(metadata, fields))
    event.warning(eventdata)

def error(eventdata, metadata=[], fields=[]):
    event = logger.bind(**__merge_context(metadata, fields))
    event.error(eventdata)

metadata = {
    'account_id': '1515323',
    'payment_plan': 'Enterprise Plus',
    'region': 'euc1',
    'feature_flags': { 'new_login': True, 'new_profile': False }
    }

info("Profile image updated", metadata, ["account_id", "feature_flags"])
