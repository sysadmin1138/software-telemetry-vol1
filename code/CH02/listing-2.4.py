# A class for wrapping the logger class, intended to provide
# a metrics-specific facility.

import sys
import json
import logging
from logging import StreamHandler

metlog  = logging.getLogger('metlog')

metfile = StreamHandler(stream=sys.stdout)

metlog.setLevel(logging.DEBUG)

metfile.propagate=False

metric_format = logging.Formatter(
        '[%(asctime)s] [metrics] %(message)s',
        )

metfile.setFormatter(metric_format)

metlog.addHandler(metfile)

def counter(msg):
    """Emits metrics intended to be counted or summarized.

    Example: counter( { "pages": 15, "words": 16272 } )
    """
    counter_metrics = {
            "counters": msg
            }
    counter_emission = json.dumps(counter_metrics)
    metlog.info(counter_emission)

