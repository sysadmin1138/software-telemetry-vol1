# A class for wrapping the logger class, intended to provide
# a metrics-specific facility.

import logging
from logging.handlers import RotatingFileHandler

metlog  = logging.getLogger('metlog')

metfile = RotatingFileHandler(
        filename='/var/log/metrics.log',
        mode="a",
        maxBytes=8*1024*1024,
        backupCount=5,
        delay=False)

metlog.setLevel(logging.DEBUG)

metfile.propagate=False

metric_format = logging.Formatter(
        '[%(asctime)s] %(message)s',
        )

metfile.setFormatter(metric_format)

metlog.addHandler(metfile)

def counter(msg, count=1):
    """Emits a metric intended to be counted or summarized.

    Example: counter("pages", "15")
    """
    metlog.info("[counter] [%s] [%s]", msg, count)

def timer(msg, time=0.0):
    """Emits a metric for tracking run-times.

    Example: timer("convert_worker_runtime", "2.7")
    """
    metlog.info("[timer] [%s] [%s]", msg, time)

