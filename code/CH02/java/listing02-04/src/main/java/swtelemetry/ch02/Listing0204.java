package swtelemetry.ch02;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * A class for wrapping the logger class, intended to provide
 * a metrics-specific facility.
 * @author Jean-François Morin
 */
public class Listing0204 {

  private static Logger METLOG = LogManager.getLogger("metlog");
  private Logger logger;
  private static TypeReference<Map<String, String>> MAP_TYPE_REFERENCE = new TypeReference<>() {};

  public Listing0204(Logger customLogger) {
    logger = customLogger;
  }

  public static void main(String[] args) {
    System.out.println("XML-based configuration");
    new Listing0204(METLOG).testLog();

    System.out.println("Programmatic configuration");
    new Listing0204(new Listing0204Logger().metlog).testLog();
  }

  public void testLog() {
    System.out.println("With double quotes");
    counter("""
            { "pages": 15, "words": 16272 }""");
    timer("""
            { "convert_worker_runtime": 2.7 }""");

    System.out.println("With single quotes");
    counter("{ 'pages': 15, 'words': 16272 }");
    timer("{ 'convert_worker_runtime': 2.7 }");
  }

  /**
   * Emits metrics intended to be counted or summarized.
   * Example: counter("{ 'pages': 15, 'words': 16272 }")
   */
  public void counter(String json) {
    Map<String, String> map = parseJson(json);
    map.forEach((k, v) -> logger.info("[counter] {} {}", k, v));
  }

  /**
   * Emits a metric for tracking run-times.
   * Example: timer("convert_worker_runtime", 2.7)
   */
  public void timer(String json) {
    Map<String, String> map = parseJson(json);
    map.forEach((k, v) -> logger.info("[timer] {} {}", k, v));
  }

  private Map<String, String> parseJson(String json) {
    try {
      ObjectMapper mapper = new ObjectMapper();
      // Allow single quotes as delimiters
      mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
      Map<String, String> map = mapper.readValue(json, MAP_TYPE_REFERENCE);
      // Parsing can also be performed as follows, but will issue
      // an unchecked type conversion warning.
      // Map<String, String> map = mapper.readValue(json, Map.class);
      return map;
    }
    catch (JsonProcessingException e) {
      logger.error(e.getMessage(), e);
      return new HashMap<>();
    }
  }

}
