package swtelemetry.ch02;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory;
import org.apache.logging.log4j.core.config.builder.api.LoggerComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.RootLoggerComponentBuilder;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;

/**
 * Programmatically configured logger.
 * @author Jean-François Morin
 */
public class Listing0203Logger {

  public Logger metlog;

  public Listing0203Logger() {
    ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();

    AppenderComponentBuilder appenderBuilder = builder.newAppender("stdout", "CONSOLE")
        .addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT);
    builder.add(appenderBuilder);

    RootLoggerComponentBuilder rootLogger = builder.newRootLogger(Level.ALL)
        .add(builder.newAppenderRef("stdout"));
    builder.add(rootLogger);

    LoggerComponentBuilder logger = builder.newLogger("metlog", Level.DEBUG)
        .add(builder.newAppenderRef("stdout"))
        .addAttribute("additivity", false);
    builder.add(logger);

    Configurator.initialize(builder.build());
    metlog = LogManager.getLogger("metlog");
  }

}
