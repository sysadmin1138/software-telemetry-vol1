package swtelemetry.ch02;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A class for wrapping the logger class, intended to provide
 * a metrics-specific facility.
 * @author Jean-François Morin
 */
public class Listing0203 {

  private static Logger METLOG = LogManager.getLogger("metlog");
  private Logger logger;

  public Listing0203(Logger customLogger) {
    logger = customLogger;
  }

  public static void main(String[] args) {
    System.out.println("XML-based configuration");
    new Listing0203(METLOG).testLog();

    System.out.println("Programmatic configuration");
    new Listing0203(new Listing0203Logger().metlog).testLog();
  }

  public void testLog() {
    counter("pages (default)");
    counter("pages", 15);

    timer("convert_worker_runtime (default)");
    timer("convert_worker_runtime", 2.7);
  }

  /**
   * Emits a metric intended to be counted or summarized.
   * Example: counter("pages", 15)
   */
  public void counter(String msg, int count) {
    logger.info("[counter] {} {}", msg, Integer.valueOf(count));
  }

  public void counter(String msg) {
    counter(msg, 1);
  }

  /**
   * Emits a metric for tracking run-times.
   * Example: timer("convert_worker_runtime", "2.7")
   */
  public void timer(String msg, double time) {
    logger.info("[timer] {} {}", msg, Double.valueOf(time));
  }

  public void timer(String msg) {
    timer(msg, 0.0);
  }

}
