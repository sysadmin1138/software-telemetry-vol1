# A class for wrapping the logger class, intended to provide
# a metrics-specific facility.

import logging
from syslog import LOG_LOCAL4
from logging.handlers import SysLogHandler

metlog  = logging.getLogger('metlog')

metfile = SysLogHandler(facility=LOG_LOCAL4)

metlog.setLevel(logging.DEBUG)

metfile.propagate=False

metric_format = logging.Formatter(
        '%(message)s',
        )

metfile.setFormatter(metric_format)

metlog.addHandler(metfile)

def counter(msg, count=1):
    """Emits a metric intended to be counted or summarized.

    Example: counter("pages", "15")
    """
    metlog.info("[counter] [%s] [%s]", msg, count)

def timer(msg, time=0.0):
    """Emits a metric for tracking run-times.

    Example: timer("convert_worker_runtime", "2.7")
    """
    metlog.info("[timer] [%s] [%s]", msg, time)

counter("pages", 15)
