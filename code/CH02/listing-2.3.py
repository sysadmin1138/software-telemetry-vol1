# A class for wrapping the logger class, intended to provide
# a metrics-specific facility.

import sys
import logging
from logging import StreamHandler

metlog  = logging.getLogger('metlog')

metfile = StreamHandler(stream=sys.stdout)

metlog.setLevel(logging.DEBUG)

metfile.propagate=False

metric_format = logging.Formatter(
        '[%(asctime)s] [metrics] %(message)s',
        )

metfile.setFormatter(metric_format)

metlog.addHandler(metfile)

def counter(msg, count=1):
    """Emits a metric intended to be counted or summarized.

    Example: counter("pages", "15")
    """
    metlog.info("[counter] [%s] [%s]", msg, count)

def timer(msg, time=0.0):
    """Emits a metric for tracking run-times.

    Example: timer("convert_worker_runtime", "2.7")
    """
    metlog.info("[timer] [%s] [%s]", msg, time)

counter("pages", 15)
timer("convert_worker_runtime", 2.7)
