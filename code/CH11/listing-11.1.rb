#!/usr/bin/env ruby
require 'benchmark'

log_examples = [
  'Added account 1141 in zone 42 with email twitterbot@example.com',
  'Account 1141 deleted from zone 37',
  'Suspended zone 42 account 1141 for excessive email volume',
  'Created new zone: 99',
  'Zone 98 deleted',
]
iter = 1000000

# Precompile RegExes to ensure we are measuring actual string-compare
# times, rather than RegEx compile time.
rbase = [
  Regexp.new('(?<acct_action>\w+) account (?<acct_id>\d+) in zone (?<zone_id>\h+) with email (?<email_address>.*)'),
  Regexp.new('Account (?<acct_id>\d+) (?<acct_action>\w+) from zone (?<zone_id>\h+)'),
  Regexp.new('(?<acct_action>\w+) zone (?<zone_id>\h+) account (?<acct_id>\d+) for (?<suspension_reason>.*)'),
  Regexp.new('(?<zone_action>\w+) new zone: (?<zone_id>\h+)'),
  Regexp.new('Zone (?<zone_id>\h+) (?<zone_action>\w+)')
]

ranchor = [
  Regexp.new('^(?<acct_action>\w+) account (?<acct_id>\d+) in zone (?<zone_id>\h+) with email (?<email_address>.*)$'),
  Regexp.new('^Account (?<acct_id>\d+) (?<acct_action>\w+) from zone (?<zone_id>\h+)$'),
  Regexp.new('^(?<acct_action>\w+) zone (?<zone_id>\h+) account (?<acct_id>\d+) for (?<suspension_reason>.*)$'),
  Regexp.new('^(?<zone_action>\w+) new zone: (?<zone_id>\h+)$'),
  Regexp.new('^Zone (?<zone_id>\h+) (?<zone_action>\w+)$')
]
Benchmark.bmbm do |rep|
  rep.report("Plain Regex-match") {
      for m in 1..iter
        for q in 0..4
          log_examples[q].match?(rbase[q])
        end
      end
  }
  rep.report("Anchored Regex-match") {
      for m in 1..iter
        for q in 0..4
          log_examples[q].match?(ranchor[q])
        end
      end
  }
  rep.report("Plain Regex-dictionary") {
    log_examples.each do |sl|
      for m in 1..iter
        rbase.each do |relist|
          sl.match?(relist)
        end
      end
    end
  }
  rep.report("Anchored Regex-dictionary") {
    log_examples.each do |sl|
      for m in 1..iter
        ranchor.each do |relist|
          sl.match?(relist)
        end
      end
    end
  }
end
