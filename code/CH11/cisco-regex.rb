#!/bin/env ruby
require 'benchmark'

log_examples = {
  'hardfail' => 'Obvious failing match',
  'halfmatch' => 'Teardown of TCP connection 113121 SYNTAX ERROR',
  'substringmatch' => 'Feb 19 02:26:26 asa1.net.prod.internal %ASA1: Teardown of UDP connection 162121 for outside:1.1.0.0/53 to dmz1:192.0.2.19/59232 duration 0:00:00 bytes 136',
  'fullmatch' => 'Teardown of UDP connection 162121 for outside:1.1.0.0/53 to dmz1:192.0.2.19/59232 duration 0:00:00 bytes 136',
  }
iter = 1000000

# Precompile RegExes to ensure we are measuring actual string-compare times,
# rather than RegEx compile time.
isimple  = Regexp.new('Teardown of (?<protocol>\w+?) connection (?<conn_id>\d+?) for (?<source>\S+?) to (?<target>\S+?) duration (?<duration>\S+?) bytes (?<bytes>\d+)$')
isource  = Regexp.new('^(?<source_int>\w+?):(?<source_ip>\S+?)/(?<source_port>\d+)$')
itarget  = Regexp.new('^(?<target_int>\w+?):(?<target_ip>\S+?)/(?<target_port>\d+)$')
icomplex = Regexp.new('Teardown of (?<protocol>\w+?) connection (?<conn_id>\d+?) for (?<source_int>\w+?):(?<source_ip>\S+?)/(?<source_port>\d+?) to (?<target_int>\w+?):(?<target_ip>\S+?)/(?<target_port>\d+?) duration (?<duration>\S+?) bytes (?<bytes>\d+)')
ianchor  = Regexp.new('^Teardown of (?<protocol>\w+?) connection (?<conn_id>\d+?) for (?<source_int>\w+?):(?<source_ip>\S+?)/(?<source_port>\d+?) to (?<target_int>\w+?):(?<target_ip>\S+?)/(?<target_port>\d+?) duration (?<duration>\S+?) bytes (?<bytes>\d+)$')
islimmed = Regexp.new('Teardown of (?<protocol>[A-Z]{3}) connection (?<conn_id>\d+?) for (?<source_int>\w{1,8}):(?<source_ip>\S{7,15})/(?<source_port>\d{1,5}) to (?<target_int>\w{4,8}):(?<target_ip>\S{7,15})/(?<target_port>\d{1,5}) duration (?<duration>\d{1,2}:\d{2}:\d{2}) bytes (?<bytes>\d+)')
islimanchor = Regexp.new('^Teardown of (?<protocol>[A-Z]{3}) connection (?<conn_id>\d+) for (?<source_int>\w{1,8}):(?<source_ip>\S{7,15})/(?<source_port>\d{1,5}) to (?<target_int>\w{4,8}):(?<target_ip>\S{7,15})/(?<target_port>\d{1,5}) duration (?<duration>\d{1,2}:\d{2}:\d{2}) bytes (?<bytes>\d+)$')

Benchmark.bmbm do |rep|
  log_examples.each do |label, sl|
    rep.report("#{label} 3pass") {
      for m in 1..iter
        regroup = sl.match(isimple)
        if regroup
          targ = regroup['target'].match?(itarget)
          sour = regroup['source'].match?(isource)
        end
      end
    }
    rep.report("#{label} 1pass") {
      for m in 1..iter
        charts = sl.match?(icomplex)
      end
    }
    rep.report("#{label} 1pass-anchor") {
      for m in 1..iter
        charts = sl.match?(ianchor)
      end
    }
    rep.report("#{label} 1pass-slim") {
      for m in 1..iter
        charts = sl.match?(islimmed)
      end
    }
    rep.report("#{label} 1pass-slimanchor") {
      for m in 1..iter
        charts = sl.match?(islimanchor)
      end
    }
  end
end

