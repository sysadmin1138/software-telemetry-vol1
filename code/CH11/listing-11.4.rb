#!/usr/bin/env ruby
require 'benchmark'

all_examples = [
  'Added account 1141 in zone 42 with email twitterbot@example.com',
  'Account 1141 deleted from zone 37',
  'Suspended zone 42 account 1141 for excessive email volume',
  'Created new zone: 99',
  'Zone 98 deleted',
 ]
 
 split_examples = [
  '[account] Added account 1141 in zone 42 with email domain example.com',
  '[account] Account 1141 deleted from zone 37',
  '[account] Suspended zone 42 account 1141 for excessive email volume',
  '[zone] Created new zone: 99',
  '[zone] Zone 98 deleted',
 
 ]
 
 unified_examples = [
   '[account] Added account 1141 in zone 42 with eamil domain example.com',
   '[account] Deleted account 1141 in zone 37',
   '[account] Suspended account 1141 in zone 42 for excessive email volume',
   '[zone] Created zone 99',
   '[zone] Deleted zone 98'
 ]
 
iter = 1000000

# Precompile RegExes to ensure we are measuring actual string-compare
# times, rather than RegEx compile time.
rall = [
  Regexp.new('^(?<acct_action>\w+?) account (?<acct_id>\d+?) in zone (?<zone_id>\h+?) with email (?<email_address>.*)$'),
  Regexp.new('^Account (?<acct_id>\d+?) (?<acct_action>\w+?) from zone (?<zone_id>\h+)$'),
  Regexp.new('^(?<acct_action>\w+?) zone (?<zone_id>\h+?) account (?<acct_id>\d+?) for (?<suspension_reason>.*)$'),
  Regexp.new('^(?<zone_action>\w+?) new zone: (?<zone_id>\h+)$'),
  Regexp.new('^Zone (?<zone_id>\h+?) (?<zone_action>\w+)$')
]

raccount = [
  Regexp.new('^\[account\] (?<acct_action>\w+?) account (?<acct_id>\d+?) in zone (?<zone_id>\h+?) with email (?<email_address>.*)$'),
  Regexp.new('^\[account\] Account (?<acct_id>\d+?) (?<acct_action>\w+?) from zone (?<zone_id>\h+)$'),
  Regexp.new('^\[account\] (?<acct_action>\w+?) zone (?<zone_id>\h+?) account (?<acct_id>\d+?) for (?<suspension_reason>.*)$'),
]

rzone = [
  Regexp.new('^\[zone\] (?<zone_action>\w+?) new zone: (?<zone_id>\h+)$'),
  Regexp.new('^\[zone\] Zone (?<zone_id>\h+?) (?<zone_action>\w+)$')
]

eaccount = Regexp.new('^\[account\] (?<acct_action>\w+?) account (?<acct_id>\d+?) in zone (?<zone_id>\h+?)\S?(?<acct_extra>.*)$')
ezone    = Regexp.new('^\[zone\] (?<zone_action>\w+?) zone (?<zone_id>\h+)$')

isaccount = Regexp.new('^\[account\]')
iszone    = Regexp.new('^\[zone\]')
Benchmark.bmbm do |rep|
  rep.report("Test all regexes") {
    all_examples.each do |sl|
      for m in 1..iter
        rall.each do |relist|
          sl.match?(relist)
        end
      end
    end
  }
  rep.report("Account/Zone split regexes") {
    split_examples.each do |sl|
      for m in 1..iter
        if sl.match?(isaccount)
          raccount.each do |relist|
            sl.match?(relist)
          end
        elsif sl.match?(iszone)
          rzone.each do |relist|
            sl.match?(relist)
          end
        end
      end
    end
  }
  rep.report("Account/Zone unified regexes") {
    unified_examples.each do |sl|
      for m in 1..iter
        if sl.match?(isaccount)
          sl.match?(eaccount)
        elsif sl.match?(iszone)
          sl.match?(ezone)
        end
      end
    end
  }
end
