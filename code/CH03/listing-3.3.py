# This will require an elasticsearch cluster to talk to
# before it will run cleanly.

import json
import redis
import time
from elasticsearch import Elasticsearch

redis_client = redis.Redis( host='log-queue.prod.internal' )
esclient = Elasticsearch (
  hosts=[{"host": "escluster.prod.internal", "port" : 9200}],
  sniff_on_start=False,
  sniffer_timeout=60
  )

wait_limit = 5

bulk_size  = 200

last_time   = time.time()
current_size  = 0
current_items = []
do_insert   = False

while True:
  counter_raw = redis_client.blpop('metrics_counters', wait_limit)
  if counter_raw == None:
    do_insert = True
  else:
    counter   = json.loads(counter_raw)
    counter['metric_type'] = 'counter'
    bulk_header = { 'index' : {
      '_index': 'metrics' } }
    bulk_item = [
        json.dump(bulk_header),
        json.dump(counter)
        ]
    current_items.append("\n".join(bulk_item))
    if len(current_items) >= bulk_size:
      do_insert = True
    elif (time.time() - last_time) >= wait_limit:
      do_insert = True

  if do_insert:
    esclient.bulk( current_items, index='metrics' )
    last_time = time.time()
    do_insert = False
    current_items = []

