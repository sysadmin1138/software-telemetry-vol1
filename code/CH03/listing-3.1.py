# A class for wrapping the elasticsearch class, intended to provide
# a metrics-specific facility.

from elasticsearch import Elasticsearch

esclient = Elasticsearch (
  hosts=[{"host": "escluster.prod.internal", "port" : 9200}],
  sniff_on_start=False,
  sniffer_timeout=60
  )

def counter(msg, count=1):
  """Emits a metric intended to be counted or summarized.

  Example: counter("pages", "15")
  """
  metric = {
    "metric_name": msg,
    "metric_value": count,
    "metric_type": "counter"
  }
  esclient.index(
    index="metrics",
    body=metric
  )

def timer(msg, time=0.0):
  """Emits a metric for tracking run-times.

  Example: timer("convert_worker_runtime", "2.7")
  """
  metric = {
    "metric_name": msg,
    "metric_value": time,
    "metric_type": "timer"
  }
  esclient.index(
    index="metrics",
    body=metric
  )

