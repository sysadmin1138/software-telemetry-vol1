# This will require a Redis server to talk to before
# it will run cleanly.

import redis

redis_client = redis.Redis( host='log-stream.prod.internal' )
redis_client.xgroup_create('syslog_stream', 'noc_team', '$')

while True:
  line = redis_client.xreadgroup(
    'noc_team',
    'noc_ingest',
    'syslog_stream',
    '>')
  do_something(line)

