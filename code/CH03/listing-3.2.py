# A class for wrapping the redis class, intended to provide
# a metrics-specific facility.

import redis
import json

redis_client = redis.Redis( host='log-queue.prod.internal')

def counter(msg, count=1):
  """Emits a metric intended to be counted or summarized.

  Example: counter("pages", "15")
  """
  metric = {
    "metric_name" : msg,
    "metric_value" : count
  }
  redis_client.rpush('metrics_counters', json.dump(metric))

def timer(msg, time=0.0):
  """Emits a metric for tracking run-times.

  Example: timer("convert_worker_runtime", "2.7")
  """
  metric = {
    "metric_name": msg,
    "metric_value": time,
  }
  redis_client.rpush('metrics_timers', json.dump(metric))

