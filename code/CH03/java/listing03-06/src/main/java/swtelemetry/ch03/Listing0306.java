package swtelemetry.ch03;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import io.honeycomb.libhoney.Event;
import io.honeycomb.libhoney.HoneyClient;
import io.honeycomb.libhoney.LibHoney;

/**
 * This Java version is executable.
 * @author Jean-François Morin
 */
public class Listing0306 {

  private HoneyClient honeyClient;

  public static void main(String[] args) {
  }

  public Listing0306() {
    String writeKey = System.getenv("HC_WRITEKEY");
    String dataset = "example.profile.pages";
    int sampleRate = 4;
    honeyClient = LibHoney.create(
        LibHoney.options().setWriteKey(writeKey)
              .setDataset(dataset)
              .setSampleRate(sampleRate)
              .build()
      );
  }

  /**
   * Main event hook.
   */
  public void doWork(File file) {
    Event hcEvent = honeyClient.createEvent();
    long fileSize = file.length();
    String filename = file.getName();
    int lastDotIndex = filename.lastIndexOf('.');
    String extension = filename.substring(lastDotIndex + 1);
    hcEvent.addField("file_size", fileSize);
    hcEvent.addField("file_extension", extension);

    List<byte[]> pngs = new ArrayList<>();

    try (PDDocument pdfDocument = PDDocument.load(file)) {
      int nbPages = pdfDocument.getNumberOfPages();
      hcEvent.addField("page_count", nbPages);

      PDFRenderer renderer = new PDFRenderer(pdfDocument);
      for (int i = 0; i < nbPages; i++) {
        BufferedImage image = renderer.renderImage(i);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(image, "png", bos);
        byte[] png = bos.toByteArray();
        pngs.add(png);
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    }

    hcEvent.addField("pages", pngs);
    hcEvent.send();
  }

}
