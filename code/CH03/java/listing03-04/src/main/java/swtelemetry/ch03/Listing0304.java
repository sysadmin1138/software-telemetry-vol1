package swtelemetry.ch03;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Protocol;
import redis.clients.jedis.StreamEntryID;

/**
 * This will require a Redis server to talk to before
 * it will run cleanly.
 * @author Jean-François Morin
 */
public class Listing0304 {

  public static void main(String[] args) {
    new Listing0304().copyToRedis();
  }

  public void copyToRedis() {
    Process process = null;
    try (
        Jedis redisClient = new Jedis(new HostAndPort("log-stream.prod.internal", Protocol.DEFAULT_PORT));
        BufferedReader reader = new BufferedReader(new FileReader("/dev/syslog_stream"))
    ) {
      process = new ProcessBuilder("mkfifo", "/dev/syslog_stream").inheritIO().start();

      while (true) {
        String line = reader.readLine();
        while (line != null) {
          redisClient.xadd("syslog_stream", StreamEntryID.NEW_ENTRY, Map.of("id", line));
          line = reader.readLine();
        }
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    finally {
      if (process != null) {
        process.destroy();
      }
    }
  }

}
