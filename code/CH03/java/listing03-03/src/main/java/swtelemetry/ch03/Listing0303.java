package swtelemetry.ch03;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.io.stream.ByteBufferStreamInput;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Protocol;

/**
 * This will require an elasticsearch cluster to talk to
 * before it will run cleanly.
 * @author Jean-François Morin
 */
public class Listing0303 {

  private RestHighLevelClient restClient;
  private int idCounter = 0;
  private Jedis redisClient;
  private static ObjectMapper mapper = new ObjectMapper();
  private static TypeReference<HashMap<String, String>> typeRef = new TypeReference<>() {};

  public Listing0303(RestHighLevelClient esClient, Jedis rClient) {
    restClient = esClient;
    redisClient = rClient;
  }

  public static void main(String[] args) {
    try (
        RestHighLevelClient esClient = new RestHighLevelClient(
            RestClient.builder(
                new HttpHost("escluster.prod.internal", 9200, "http"))
            );
        Jedis rClient = new Jedis(new HostAndPort("log-queue.prod.internal", Protocol.DEFAULT_PORT));
    ) {
      new Listing0303(esClient, rClient).test();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void test() {
    int waitLimit = 5;
    int bulkSize = 200;

    Instant lastTime = Instant.now();
    List<String> currentItems = new ArrayList<>();
    boolean doInsert = false;

    while (true) {
      List<String> counterRaw = redisClient.blpop(waitLimit, "metrics_counters");

      if (counterRaw == null || counterRaw.isEmpty()) {
        doInsert = true;
      } else {
        try {
          Map<String, String> counter = mapper.readValue(
              counterRaw.stream().collect(Collectors.joining("\n")),
              typeRef);
          counter.put("metric_type", "counter");
          Map<String, Map<String, String>> bulkHeader = Map.of(
              "index", Map.of("index", "metrics")
          );
          List<Map<String, ?>> bulkItem = new ArrayList<>();
          bulkItem.add(bulkHeader);
          bulkItem.add(counter);
          currentItems.add(bulkItem.stream()
              .map(Listing0303::jsonToString)
              .collect(Collectors.joining("\n")));

          if (currentItems.size() >= bulkSize) {
            doInsert = true;
          } else if (Instant.now().getEpochSecond() - lastTime.getEpochSecond() >= waitLimit) {
            doInsert = true;
          }

          if (doInsert) {
            BulkRequest bulkRequest = new BulkRequest("metrics");
            for (String item : currentItems) {
              IndexRequest indexRequest = new IndexRequest(new ByteBufferStreamInput(ByteBuffer.wrap(item.getBytes())));
              indexRequest.index("metrics");
              indexRequest.id("" + (++idCounter));
              bulkRequest.add(indexRequest);
            }
            restClient.bulk(bulkRequest, RequestOptions.DEFAULT);

            lastTime = Instant.now();
            doInsert = false;
            currentItems = new ArrayList<>();
          }
        }
        catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private static String jsonToString(Map<String, ?> json) {
    try {
      return mapper.writeValueAsString(json);
    }
    catch (JsonProcessingException e) {
      throw new IllegalArgumentException(e.getMessage(), e);
    }
  }

}
