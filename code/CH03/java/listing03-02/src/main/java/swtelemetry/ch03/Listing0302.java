package swtelemetry.ch03;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Protocol;

/**
 * A class for wrapping the redis class, intended to provide
 * a metrics-specific facility.
 * @author Jean-François Morin
 */
public class Listing0302 {

  private Jedis redisClient;

  public Listing0302(Jedis client) {
    redisClient = client;
  }

  public static void main(String[] args) {
    try (Jedis client = new Jedis(new HostAndPort("log-queue.prod.internal", Protocol.DEFAULT_PORT))) {
      new Listing0302(client).testLog();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void testLog() {
    counter("pages (default)");
    counter("pages", 15);

    timer("convert_worker_runtime (default)");
    timer("convert_worker_runtime", 2.7);
  }

  /**
   * Emits a metric intended to be counted or summarized.
   * Example: counter("pages", 15)
   */
  public void counter(String msg, int count) {
    String metric = String.format("""
        {
          "metric_name", %s,
          "metric_value", %s
        }""", msg, count);
    redisClient.rpush("metrics_counters", metric);
  }

  public void counter(String msg) {
    counter(msg, 1);
  }

  /**
   * Emits a metric for tracking run-times.
   * Example: timer("convert_worker_runtime", 2.7)
   */
  public void timer(String msg, double time) {
    String metric = String.format("""
        {
          "metric_name", %s,
          "metric_value", %s
        }""", msg, time);
    redisClient.rpush("metrics_timers", metric);
  }

  public void timer(String msg) {
    timer(msg, 0.0);
  }

}
