package swtelemetry.ch03;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Protocol;
import redis.clients.jedis.StreamEntry;
import redis.clients.jedis.StreamEntryID;
import redis.clients.jedis.params.XReadGroupParams;

/**
 * This will require a Redis server to talk to before
 * it will run cleanly.
 * @author Jean-François Morin
 */
public class Listing0305 {

  private Consumer<List<Map.Entry<String, List<StreamEntry>>>> doSomething;

  public static void main(String[] args) {
    Consumer<List<Map.Entry<String, List<StreamEntry>>>> doSomething = System.out::println;
    new Listing0305(doSomething).readAndAppend();
  }

  public Listing0305(Consumer<List<Map.Entry<String, List<StreamEntry>>>> action) {
    doSomething = action;
  }

  public void readAndAppend() {
    try (
        Jedis redisClient = new Jedis(new HostAndPort("log-stream.prod.internal", Protocol.DEFAULT_PORT));
    ) {
      redisClient.xgroupCreate("syslog_stream", "noc_team", StreamEntryID.LAST_ENTRY, false);
      while (true) {
        List<Map.Entry<String, List<StreamEntry>>> line = redisClient.xreadGroup(
            "noc_team",
            "noc_ingest",
            XReadGroupParams.xReadGroupParams(),
            Map.of("syslog_stream", StreamEntryID.UNRECEIVED_ENTRY)
        );
        doSomething.accept(line);
      }
    }
  }

}
