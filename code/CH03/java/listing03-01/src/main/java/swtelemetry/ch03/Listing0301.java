package swtelemetry.ch03;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;

import org.apache.http.HttpHost;
import org.elasticsearch.action.DocWriteResponse.Result;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.io.stream.ByteBufferStreamInput;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * A class for wrapping the elasticsearch class, intended to provide
 * a metrics-specific facility.
 * @author Jean-François Morin
 */
public class Listing0301 {

  private RestHighLevelClient restClient;
  private int idCounter = 0;
  private ObjectMapper mapper = new ObjectMapper();

  public Listing0301(RestHighLevelClient client) {
    restClient = client;
  }

  public static void main(String[] args) {
    try (
        RestHighLevelClient client = new RestHighLevelClient(
            RestClient.builder(
                new HttpHost("escluster.prod.internal", 9200, "http"))
            );
    ) {
      new Listing0301(client).testLog();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void testLog() {
    counter("pages (default)");
    counter("pages", 15);

    timer("convert_worker_runtime (default)");
    timer("convert_worker_runtime", 2.7);
  }

  /**
   * Emits a metric intended to be counted or summarized.
   * Example: counter("pages", 15)
   */
  public void counter(String msg, int count) {
    Map<String, Object> metric = Map.of(
        "metric_name", msg,
        "metric_value", count,
        "metric_type", "counter"
    );
    IndexResponse indexResponse = doIndex(metric);
    if (indexResponse.getResult() == Result.CREATED) {
      System.out.println("Metric created");
    } else if (indexResponse.getResult() == Result.UPDATED) {
      System.out.println("Metric updated");
    }
  }

  public void counter(String msg) {
    counter(msg, 1);
  }

  /**
   * Emits a metric for tracking run-times.
   * Example: timer("convert_worker_runtime", 2.7)
   */
  public void timer(String msg, double time) {
    Map<String, Object> metric = Map.of(
        "metric_name", msg,
        "metric_value", time,
        "metric_type", "timer"
    );
    IndexResponse indexResponse = doIndex(metric);
    if (indexResponse.getResult() == Result.CREATED) {
      System.out.println("Metric created");
    } else if (indexResponse.getResult() == Result.UPDATED) {
      System.out.println("Metric updated");
    }
  }

  public void timer(String msg) {
    timer(msg, 0.0);
  }

  private IndexResponse doIndex(Map<String, ?> document) {
    try {
      String json = mapper.writeValueAsString(document);
      IndexRequest request = new IndexRequest(new ByteBufferStreamInput(ByteBuffer.wrap(json.getBytes())));
      request.index("metrics");
      request.id("" + (++idCounter));
      IndexResponse indexResponse = restClient.index(request, RequestOptions.DEFAULT);
      return indexResponse;
    }
    catch (IOException e) {
      throw new IllegalStateException(e.getMessage(), e);
    }
  }

}
