# This is not intended to be executable.

import libhoney
import [lots of other things]

libhoney.init(
  writekey=os.getenv('HC_WRITEKEY'),
  dataset='example.profile.pages',
  sample_rate=4 )

def get_file_details(options):
  [...]

def get_page_details(file_details):
  [...]

def enqueue_pdf_create(page_details):
  [...]

def wait_png_pages(png_pages):
  [...]

# Main event hook.
def do_work(options):
  hc_event = libhoney.new_event()
  file_details = get_file_details(options)
  hc_event.add_field('file_size', file_details['file_size'])
  hc_event.add_field('file_extension', file_details['extension'])
  
  page_details = get_page_details(file_details)
  hc_event.add_field('page_count', page_details['count'])

  png_pages = enqueue_png_create(page_details)

  wait_png_pages(png_pages)
  hc_event.send()
