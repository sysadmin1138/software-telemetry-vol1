# This will require a Redis server to talk to before
# it will run cleanly.

import os
import redis

os.mkfifo('/dev/syslog_stream')

redis_client = redis.Redis( host='log-stream.prod.internal')

rsyslog_stream = open('/dev/syslog_stream', "r")
while True:
  for line in rsyslog_stream:
    redis_client.xadd(
      'syslog_stream',
      '*',
      line )
