package swtelemetry.ch06;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.stream.Collectors;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Protocol;

/**
 * A class for wrapping the redis class, intended to provide
 * a metrics-specific facility.
 * @author Jean-François Morin
 */
public class Listing0601 {

  private static final String VERSION = "2025.02.19.8cd321b9";
  private Jedis redisClient = new Jedis(new HostAndPort("log-stream.prod.internal", Protocol.DEFAULT_PORT));

  public Map<String, Object> contextTelemetry(Map<String, ?> metadata) {
    String hostname = "";
    try {
      hostname = InetAddress.getLocalHost().getHostName();
    }
    catch (UnknownHostException e) {
      e.printStackTrace();
    }
    return Map.ofEntries(
        Map.entry("hostname", hostname),
        Map.entry("pid", ProcessHandle.current().pid()),
        Map.entry("version_id", VERSION),
        Map.entry("payment_plan", metadata.get("payment_plan"))
    );
  }

  public void testLog() {
    Map<String, Object> metadata = Map.of("payment_plan", "credit card");
    counter("pages (default)", metadata);
    counter("pages", metadata, 15);

    timer("convert_worker_runtime (default)", metadata);
    timer("convert_worker_runtime", metadata, 2.7);
  }

  /**
   * Emits a metric intended to be counted or summarized.
   * Example: counter("pages", 15)
   */
  public void counter(String msg, Map<String, ?> metadata, int count) {
    Map<String, Object> metric = Map.ofEntries(
        Map.entry("metric_name", msg),
        Map.entry("metric_value", count)
    );
    Map<String, Object> context = contextTelemetry(metadata);
    metric.putAll(context);
    String jsonMetric = context.entrySet().stream()
        .map(e -> String.format("\"%s\": \"%s\"", e.getKey(), e.getValue()))
        .collect(Collectors.joining(",\n", "{", "}"));
    redisClient.rpush("metrics_counters", jsonMetric);
  }

  public void counter(String msg, Map<String, ?> metadata) {
    counter(msg, metadata, 1);
  }

  /**
   * Emits a metric for tracking run-times.
   * Example: timer("convert_worker_runtime", 2.7)
   */
  public void timer(String msg, Map<String, Object> metadata, double time) {
    Map<String, Object> metric = Map.ofEntries(
        Map.entry("metric_name", msg),
        Map.entry("metric_value", time)
    );
    Map<String, Object> context = contextTelemetry(metadata);
    metric.putAll(context);
    String jsonMetric = context.entrySet().stream()
        .map(e -> String.format("\"%s\": \"%s\"", e.getKey(), e.getValue()))
        .collect(Collectors.joining(",\n", "{", "}"));
    redisClient.rpush("metrics_timers", jsonMetric);
  }

  public void timer(String msg, Map<String, Object> metadata) {
    timer(msg, metadata, 0.0);
  }

}
