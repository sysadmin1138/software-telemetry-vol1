package swtelemetry.ch06;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.api.trace.propagation.W3CTraceContextPropagator;
import io.opentelemetry.context.propagation.ContextPropagators;
import io.opentelemetry.exporter.otlp.trace.OtlpGrpcSpanExporter;
import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.trace.SdkTracerProvider;
import io.opentelemetry.sdk.trace.export.BatchSpanProcessor;

/**
 * @author Jean-François Morin
 */
public class Listing0602 {

  private Tracer tracer;
  private String hostname;
  private long pid;

  public Listing0602() {
    SdkTracerProvider sdkTracerProvider = SdkTracerProvider.builder()
        .addSpanProcessor(BatchSpanProcessor.builder(OtlpGrpcSpanExporter.builder().build()).build())
        .build();

    OpenTelemetry openTelemetry = OpenTelemetrySdk.builder()
        .setTracerProvider(sdkTracerProvider)
        .setPropagators(ContextPropagators.create(W3CTraceContextPropagator.getInstance()))
        .buildAndRegisterGlobal();
    tracer = openTelemetry.getTracer("instrumentation-library-name", "1.0.0");
    try {
      hostname = InetAddress.getLocalHost().getHostName();
    }
    catch (UnknownHostException e) {
      e.printStackTrace();
    }
    pid = ProcessHandle.current().pid();
  }

  /**
   * Called by queue system
   */
  public void perform(Map<String, String> options) {
    Attributes attributes = Attributes.builder()
        .put("session_id", options.get("session_id"))
        .put("document_id", options.get("document_id"))
        .put("process_id", pid)
        .put("account_id", options.get("account_id"))
        .put("host", hostname)
        .build();
    Span span = null;
    try {
      span = tracer.spanBuilder("pdf_pages")
          .setAllAttributes(attributes)
          .startSpan();
      // TODO: To be implemented. See listing 3.6 for an example
      // of conversion of PDF pages to PNG.
      //pages = convertPages(options);
      //metrics.counter("pdf_pages", pages);
    }
    finally {
      if (span != null) {
        span.end();
      }
    }
  }

}
