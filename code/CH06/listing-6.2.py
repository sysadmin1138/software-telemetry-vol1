import os
import socket
from opentelemetry import trace

tracer   = trace.get_tracer(__name__)
hostname = socket.gethostname()
pid      = os.getpid()

# Called by queue system
def perform(options):
  attributes = {
    "session_id" : options[session_id],
    "document_id" : options[document_id],
    "process_id" : pid,
    "account_id" : options[account_id],
    "host" : hostname
  }
  with tracer.start_as_current_span("pdf_pages",
                                    attributes=attributes):
    pages = convertPages(options)
    metrics.counter("pdf_pages", pages)
  # end trace
# end perform()
