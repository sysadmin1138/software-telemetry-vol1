# A class for wrapping the redis class, intended to provide
# a metrics-specific facility.

import redis
import json
import socket
import os
from version import __version__

redis_client = redis.Redis( host='log-queue.prod.internal')

def __context_telemetry():
  context = {
    "hostname" : socket.gethostname(),
    "pid" : os.getpid(),
    "version_id" : __version__,
    "payment_plan" : metadata['payment_plan']
  }
  return context

def counter(msg, metadata, count=1):
    """Emits a metric intended to be counted or summarized.

    Example: counter("pages", metadata, "15")
    """
    base_metric = {
      "metric_name" : msg,
      "metric_value" : count
    }
    context = __context_telemetry(message)
    metric = { **base_metric, **context }
    redis_client.rpush('metrics_counters', json.dump(metric))

def timer(msg, metadata, count=1):
    """Emits a metric for tracking run-times.

    Example: timer("convert_worker_runtime", metadata, "2.7")
    """
    base_metric = {
      "metric_name" : msg,
      "metric_value" : time
      }
    context = __context_telemetry(message)
    metric = { **base_metric, **context }
    redis_client.rpush('metrics_timers', json.dump(metric))

