import socket

host = "127.0.1.2"
port = 9201

telemetry_in = socket.socket(
        socket.AF_INET,
        socket.SOCK_DGRAM)

telemetry_in.bind((host, port))

while 1:
    data, addr = telemetry_in.recvfrom(8192)
    try:
        value = data.decode('utf8')
        print(value)
    except:
        print("Invalid data received.")


telemetry_in.close()
    
