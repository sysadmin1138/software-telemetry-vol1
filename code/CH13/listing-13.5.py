from cryptography.fernet import Fernet
import socket
import json
import base64

host = "127.0.1.2"
port = 9201

badkey = b'this is a bad key -- do not use.'
decrypter = Fernet(base64.b64encode(badkey))
telemetry_in = socket.socket(
        socket.AF_INET,
        socket.SOCK_DGRAM)

telemetry_in.bind((host, port))

while 1:
    data, addr = telemetry_in.recvfrom(8192)
    try:
        value = data.decode('utf8')
        print('Encrypted text: %s ' % value)
        enc_event = json.loads(value)
        b64event = base64.b64decode(enc_event['event'])
        try:
            event = decrypter.decrypt(b64event).decode('utf8')
        except:
            print('Failed decryption')
        print('Clear text: %s' % event)
    except:
        print("Invalid data received.")


telemetry_in.close()
    
