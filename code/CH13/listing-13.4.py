# Formerly listing 12.6
import logging
import datetime
import sys
import socket
import os
import json
from internalrenderer import (InternalRenderer)
from encrypter import (Encrypter)
from structlog import (
    get_logger,
    configure,
)
from structlog.stdlib import (
    LoggerFactory,
    BoundLogger,
    add_log_level,
)
from structlog.processors import (
    JSONRenderer,
    UnicodeDecoder,
    TimeStamper
)
from logging.handlers import (
    DatagramHandler,
)

class PlainDatagramHandler(DatagramHandler):
    def emit(self, record):
        try:
            s = record.msg
            self.send(s.encode('utf8'))
        except Exception:
            self.handleError(record)

logging.basicConfig(
    format="%(message)s",
    handlers=[
        PlainDatagramHandler('127.0.1.2', 9201)
    ],
    level=logging.INFO,
)

__release__ = "0.7.1"
__commit__ = "f0d00b1"

configure(
    processors=[
        TimeStamper(fmt="iso"),
        UnicodeDecoder(),
        add_log_level,
        InternalRenderer(),
        Encrypter(),
    ],
    context_class=dict,
    logger_factory=LoggerFactory(),
    wrapper_class=BoundLogger,
    cache_logger_on_first_use=False
)

logger = get_logger()

def __add_context():
    context = {
        'hostname': socket.gethostname(),
        'pid': os.getpid(),
        'release': __release__,
        'commit': __commit__
    }
    return context

def __filter_metadata(metadata, fields):
    fcontext = {}
    for f in fields:
        if f in metadata:
            fcontext[f] = metadata[f]

    return fcontext

def __merge_context(metadata, fields):
    base_context = __add_context()
    filter_context = __filter_metadata(metadata, fields)
    merged_context = {**base_context, **filter_context}
    return merged_context

def info(event, metadata=[], fields=[]):
    log = logger.bind(**__merge_context(metadata, fields))
    log.info(event)

def warning(event, metadata=[], fields=[]):
    log = logger.bind(**__merge_context(metadata, fields))
    log.warning(event)

def error(event, metadata=[], fields=[]):
    log = logger.bind(**__merge_context(metadata, fields))
    log.error(event)

metadata = {
    'account_id': '1515323',
    'payment_plan': 'Enterprise Plus',
    'region': 'euc1',
    'feature_flags': { 'new_login': True, 'new_profile': False }
    }

info("Profile image updated", metadata, ["account_id", "feature_flags"])
