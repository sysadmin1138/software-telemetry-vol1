import json

class InternalRenderer(object):

    def __init__(self, serializer=json.dumps):
        self._dumps = serializer

    def __call__(self, logger, name, event_dict):
        timestamp = event_dict.pop('timestamp')
        event = event_dict.pop('event')
        context_json = self._dumps(event_dict)
        return timestamp + " " + event + " -- " + context_json

