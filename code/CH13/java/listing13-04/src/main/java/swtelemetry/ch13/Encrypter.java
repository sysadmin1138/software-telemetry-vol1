package swtelemetry.ch13;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Map;

import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.Node;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginBuilderAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginBuilderFactory;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.core.layout.AbstractStringLayout;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.message.Message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.macasaet.fernet.Key;

/**
 * @author Jean-François Morin
 */
@Plugin(name = "FernetEncrypterLayout", category = Node.CATEGORY, elementType = Layout.ELEMENT_TYPE, printObject = true)
public class Encrypter extends AbstractStringLayout {

  private byte[] key;
  private String keyVersion;
  private ObjectMapper jsonSerializer = new ObjectMapper();

  public static class Builder<B extends Builder<B>> extends AbstractStringLayout.Builder<B>
  implements org.apache.logging.log4j.core.util.Builder<Encrypter> {

    @PluginBuilderAttribute
    private String key;
    @PluginBuilderAttribute
    private String keyVersion = "1.0";

    public Builder<B> setKey(String key) {
      this.key = key;
      return this;
    }

    public Builder<B> setKeyVersion(String keyVersion) {
      this.keyVersion = keyVersion;
      return this;
    }

    protected String toStringOrNull(final byte[] header) {
      return header == null ? null : new String(header, Charset.defaultCharset());
    }

    @Override
    public Encrypter build() {
      String headerPattern = toStringOrNull(getHeader());
      String footerPattern = toStringOrNull(getFooter());
      return new Encrypter(key, keyVersion, getConfiguration(), headerPattern, footerPattern, getCharset());
    }

  }

  private Encrypter(String bkey, String keyVersion, Configuration config,
      String headerPattern, String footerPattern, Charset charset) {
    super(config, charset,
        PatternLayout.newSerializerBuilder().setConfiguration(config).setPattern(headerPattern).setDefaultPattern("").build(),
        PatternLayout.newSerializerBuilder().setConfiguration(config).setPattern(footerPattern).setDefaultPattern("").build());
    key = Base64.getEncoder().encode(bkey.getBytes());
    this.keyVersion = keyVersion;
  }

  @PluginBuilderFactory
  public static <B extends Builder<B>> B newBuilder() {
    return new Builder<B>().asBuilder();
  }

  @Override
  public String toSerializable(LogEvent event) {
    Log4jLogEvent log4jEvent = convertMutableToLog4jEvent(event);
    Message message = log4jEvent.getMessage();
    String payload = message.getFormattedMessage();

    Key cipher = new Key(key);
    byte[] safeEvent = cipher.encrypt(payload.getBytes(), null);
    Map<String, Object> encEvent = Map.of("kver", keyVersion, "event", Base64.getEncoder().encodeToString(safeEvent));
    try {
      return jsonSerializer.writeValueAsString(encEvent);
    }
    catch (JsonProcessingException e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  private static Log4jLogEvent convertMutableToLog4jEvent(LogEvent event) {
    return (Log4jLogEvent) (event instanceof Log4jLogEvent ? event : Log4jLogEvent.createMemento(event));
  }

}
