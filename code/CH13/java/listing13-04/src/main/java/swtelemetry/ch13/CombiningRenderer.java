package swtelemetry.ch13;

import java.nio.charset.Charset;

import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.Node;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginBuilderAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginBuilderFactory;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.core.layout.AbstractStringLayout;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.message.SimpleMessage;

@Plugin(name = "FernetEncryptedInternalRendererLayout", category = Node.CATEGORY, elementType = Layout.ELEMENT_TYPE, printObject = true)
public class CombiningRenderer extends AbstractStringLayout {

  private InternalRenderer internalRenderer;
  private Encrypter encrypter;

  public static class Builder<B extends Builder<B>> extends AbstractStringLayout.Builder<B> implements org.apache.logging.log4j.core.util.Builder<CombiningRenderer> {

    @PluginBuilderAttribute
    private boolean compact;
    @PluginBuilderAttribute
    private String key;
    @PluginBuilderAttribute
    private String keyVersion = "1.0";

    protected String toStringOrNull(final byte[] header) {
      return header == null ? null : new String(header, Charset.defaultCharset());
    }

    @Override
    public CombiningRenderer build() {
      String headerPattern = toStringOrNull(getHeader());
      String footerPattern = toStringOrNull(getFooter());
      InternalRenderer.Builder<?> irb = new InternalRenderer.Builder<>()
          .setCompact(compact);
      InternalRenderer internalRenderer = irb.build();

      Encrypter.Builder<?> eb = new Encrypter.Builder<>()
          .setKey(key)
          .setKeyVersion(keyVersion);
      Encrypter encrypter = eb.build();

      return new CombiningRenderer(internalRenderer, encrypter, getConfiguration(), headerPattern, footerPattern, getCharset());
    }

  }

  private CombiningRenderer(InternalRenderer internalRenderer, Encrypter encrypter, Configuration config,
      String headerPattern, String footerPattern, Charset charset) {
    super(config, charset,
        PatternLayout.newSerializerBuilder().setConfiguration(config).setPattern(headerPattern).setDefaultPattern("").build(),
        PatternLayout.newSerializerBuilder().setConfiguration(config).setPattern(footerPattern).setDefaultPattern("").build());
    this.internalRenderer = internalRenderer;
    this.encrypter = encrypter;
  }

  @PluginBuilderFactory
  public static <B extends Builder<B>> B newBuilder() {
    return new Builder<B>().asBuilder();
  }

  @Override
  public String toSerializable(LogEvent event) {
    String internal = internalRenderer.toSerializable(event);

    Log4jLogEvent.Builder evtBuilder = new Log4jLogEvent.Builder(event);
    evtBuilder.setMessage(new SimpleMessage(internal));

    return encrypter.toSerializable(evtBuilder.build());
  }

}
