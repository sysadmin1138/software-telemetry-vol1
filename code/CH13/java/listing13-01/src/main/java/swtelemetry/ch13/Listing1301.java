package swtelemetry.ch13;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.StandardSocketOptions;
import java.nio.charset.Charset;

/**
 * @author Jean-François Morin
 */
public class Listing1301 {

  public static void main(String[] args) {
    String host = "127.0.1.2";
    int port = 9201;

    try (Socket telemetryIn = new Socket(host, port);
        InputStream istream = telemetryIn.getInputStream()) {
      telemetryIn.setOption(StandardSocketOptions.SO_RCVBUF, Integer.valueOf(8192));
      byte[] buffer = new byte[8192];
      while (true) {
        int bytesRead = istream.read(buffer);
        if (bytesRead > 0) {
          String value = new String(buffer, 0, bytesRead, Charset.forName("UTF-8"));
          System.out.print(value);
        }
      }
    }
    catch (IOException e) {
      System.err.println("Invalid data received.");
      e.printStackTrace();
    }
  }

}
