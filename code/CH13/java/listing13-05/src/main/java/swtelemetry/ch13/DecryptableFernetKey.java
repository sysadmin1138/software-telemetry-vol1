package swtelemetry.ch13;

import javax.crypto.spec.IvParameterSpec;

import com.macasaet.fernet.Key;

/**
 * Fernet Key subclass that makes the {@link #decrypt(byte[], IvParameterSpec)}
 * method public so that it can be used in class Listing1305.
 * @author Jean-François Morin
 */
public class DecryptableFernetKey extends Key {

  public DecryptableFernetKey(byte[] signingKey, byte[] encryptionKey) {
    super(signingKey, encryptionKey);
  }

  public DecryptableFernetKey(byte[] concatenatedKeys) {
    super(concatenatedKeys);
  }

  public DecryptableFernetKey(String string) {
    super(string);
  }

  @Override
  public byte[] decrypt(byte[] cipherText, IvParameterSpec initializationVector) {
    return super.decrypt(cipherText, initializationVector);
  }

}
