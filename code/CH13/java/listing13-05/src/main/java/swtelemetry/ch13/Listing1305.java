package swtelemetry.ch13;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.StandardSocketOptions;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Jean-François Morin
 */
public class Listing1305 {

  public static void main(String[] args) {
    String host = "127.0.1.2";
    int port = 9201;

    byte[] badKey = "this is a bad key -- do not use.".getBytes();
    DecryptableFernetKey cipher = new DecryptableFernetKey(badKey);

    ObjectMapper jsonSerializer = new ObjectMapper();

    try (Socket telemetryIn = new Socket(host, port);
        InputStream istream = telemetryIn.getInputStream()) {
      telemetryIn.setOption(StandardSocketOptions.SO_RCVBUF, Integer.valueOf(8192));
      byte[] buffer = new byte[8192];
      while (true) {
        int bytesRead = istream.read(buffer);
        byte[] toDecrypt = buffer;
        if (bytesRead < buffer.length) {
          toDecrypt = new byte[bytesRead];
          System.arraycopy(buffer, 0, toDecrypt, 0, bytesRead);
        }
        String value = new String(toDecrypt, Charset.forName("UTF-8"));
        System.out.printf("Encrypted text: %s%n", value);

        @SuppressWarnings("unchecked")
        Map<String, Object> encEvent = jsonSerializer.readValue(toDecrypt, Map.class);
        byte[] b64event = Base64.getDecoder().decode((byte[]) encEvent.get("event"));
        try {
          byte[] event = cipher.decrypt(b64event, null);
          System.out.printf("Clear text: %s%n", new String(event));
        }
        catch (Exception e) {
          System.out.println("Failed decryption");
        }
      }
    }
    catch (IOException e) {
      System.err.println("Invalid data received.");
      e.printStackTrace();
    }
  }

}
