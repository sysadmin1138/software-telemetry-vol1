from cryptography.fernet import Fernet
import json
import base64

class Encrypter(object):

    def __init__(self):
        bkey = b'this is a bad key -- do not use.'
        self._key = base64.b64encode(bkey)
        self._key_version = '1.0'

    def __call__(self, logger, name, event_dict):
        enc_event = { 'kver': self._key_version }
        cipher = Fernet(self._key)
        safe_event = cipher.encrypt(event_dict.encode('utf8'))
        enc_event['event'] = base64.b64encode(safe_event).decode('utf8')
        return json.dumps(enc_event)

