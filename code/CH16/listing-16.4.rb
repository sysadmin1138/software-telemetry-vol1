require 'elasticsearch'
require 'rest-client'
require 'json'

ES_HOST = 'logger.es.prod'
PROMTAIL_HOST = 'logger.promtail.prod'
BULK_SIZE = 100

esclient = Elasticsearch::Client.new host: ES_HOST

def reindex(source_idx)
  result_set = esclient.search(
    index: source_idx,
    search_type: 'scan',
    scroll: '2m',
    sort: ['_timestamp,asc'])

  uri = "http://#{PROMTAIL_HOST}:8080/loki/api/v1/push"
  loki_base = { "streams": [ { 
    "stream": { "label": "exampleapp" },
    "values": []
  } ] }
  doc_count = 0
  update_set = []
  loop do
    result_set = esclient.scroll(
      scroll_id: result_set['_scroll_id'],
      scroll: '2m')
    break if result_set['hits']['hits'].empty?
    result_set['hits']['hits'].each do |doc|
      doc_count = doc_count + 1
      telemetry = doc['fields']
      # Redactions, if any, go here
      update_set.push = [
        doc['_timestamp'],
        JSON.dump(telemetry) ]
    end
    if doc_count >= BULK_SIZE
      loki_update = loki_base
      loki_update['streams'][0]['values'] = update_set
      RestClient.post(
        uri,
        JSON.dump( loki_update ),
        :content_type => 'application/json' )
      update_set = []
      doc_count = 0
    end
  end
  loki_update = loki_base
  loki_update['streams'][0]['values'] = update_set
  RestClient.post(
    uri,
    JSON.dump( loki_update ),
    :content_type => 'application/json' )
end
