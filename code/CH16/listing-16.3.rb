require 'elasticsearch'

# Host to perform serches against.
ES_HOST   = 'logger.es.prod'

# The bulk-update size limit. Change this to tune the impact
# to ingestion.
BULK_SIZE = 1000

esclient = Elasticsearch::Client.new host: ES_HOST

# Start our search, using pagination (scan/scroll). This
# returns _scroll_id, which we will use in the loop below.
result_set = esclient.search(
  index: 'filebeat-2023.02.19',
  search_type: 'scan',
  scroll: '2m',
  q: 'message:"Invalid API payload" AND message:"username"' )

doc_count  = 0
update_set = []

# Loops actions:
# 1: Fetch results using the _scroll_id value.
# 2: If result_set is empty (no more documents), break.
# 3: If we get documents, redact them.
# 4: if the update_set has reached BULK_SIZE, send update_set
loop do
  result_set = esclient.scroll(
    scroll_id: result_set['_scroll_id'],
    scroll: '2m')
  break if result_set['hits']['hits'].empty?
  result_set['hits']['hits'].each do |doc|
    doc_count = doc_count + 1
    clean_mess = doc['fields']['message'][0].split(':')
    update_set.push( {
      update: {
        _index: doc['_index'],
        _id: doc['_id'],
        data: {
          message: clean_mess[0]
        }
      } } )
  end
  if doc_count >= BULK_SIZE
    esclient.bulk body: update_set
    doc_count = 0
    update_set = []
  end
end
esclient.bulk body: update_set
