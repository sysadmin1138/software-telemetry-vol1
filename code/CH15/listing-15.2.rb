def filter(event)
  msg  = event.get('message')
  hmsg = event.get('message_256')
  Thread.current['message_check'] ||= OpenSSL::Digest::SHA256.new
  hasher = Thread.current['message_check']
  test = hasher.hexdigest(msg.to_s)
  if hmsg == test
    event.set('message_valid', true)
  else
    event.set('message_valid', false)
  end
  return [event]
end
